!
! Using a leak map 
!

(config)# access-list 10 permit 10.10.77.0/24
(config)# route-map RM_LEAKMAP permit 10
(config-route-map)# match ip address 10

!
!
!
(config)# int s1/1
(config-if)# ip summary-address eigrp 100 10.0.0.0 255.0.0. leak-map RM_LEAKMAP

!
!
!
(config)# router eigrp 100
(config-router) eigrp stub leak-map RM-LEAKMAP


